//
//  PopupPolicyViewController.swift
//  DemoLogin
//
//  Created by Atthachai Meethong on 10/20/15.
//  Copyright © 2015 Atthachai Meethong. All rights reserved.
//

import UIKit

class PopupPolicyViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    let font: UIFont! = UIFont(name: "DINEngschriftStd", size: 20.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.font = self.font
        self.titleLabel.textColor = UIColor.blackColor()
        
        self.textView.font = self.font
        self.textView.textColor = UIColor.darkGrayColor()
        self.textView.backgroundColor = UIColor.clearColor()
        
        let fileURL = NSBundle.mainBundle().URLForResource("PRIVACY-POLICY", withExtension: "txt")!
        let text = try! String(contentsOfURL: fileURL, encoding: NSUTF8StringEncoding)
        self.textView.text = text
        
        self.textView.scrollRangeToVisible(NSMakeRange(0, 1))
    
    }
    

    @IBAction func pushOnCloseButton(sender: UIButton) {
        self.dismissPopupViewController(.Fade, view: self.view)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
