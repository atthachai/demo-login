//
//  PopupPolicyViewController.swift
//  DemoLogin
//
//  Created by Atthachai Meethong on 10/16/15.
//  Copyright © 2015 Atthachai Meethong. All rights reserved.
//

import UIKit

@objc protocol PopupDelegate {
    optional func popupPressSubmit(sender: PopupSubmitViewController)
    optional func popupPressClose(sender: UIViewController)
}

class PopupSubmitViewController: UIViewController , PopupDelegate{
    
     var delegate:PopupDelegate?
    
    @IBOutlet weak var privacyPolicyButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var acceptPolicyButton: UIButton!
    @IBOutlet weak var getExclusiveButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var agreeSentence: UILabel!
    @IBOutlet weak var getExclusiveSentence: UILabel!

    var isAcceptPolicy:Bool = false
    var isGetExclusive:Bool = false
    
    let font: UIFont! = UIFont(name: "DINEngschriftStd", size: 20.0)
    let font2: UIFont! = UIFont(name: "DINEngschriftStd", size: 18.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.submitButton.userInteractionEnabled = false
        self.setTheme()
    }
    
    @IBAction func pushOnPrivacyPolicyLink(sender: UIButton) {
        self.displayPopupPolicy()
    }
    
    @IBAction func pushOnAcceptPolicyButton(sender: UIButton) {
        if self.isAcceptPolicy {
            self.acceptPolicyButton.backgroundColor = UIColor.lightGrayColor()
        } else {
            self.acceptPolicyButton.backgroundColor = UIColor.blackColor()
        }
        self.isAcceptPolicy = !self.isAcceptPolicy
        self.checkAllowSubmit()
    }
    @IBAction func pushOnGetExclusive(sender: UIButton) {
        if self.isGetExclusive {
            self.getExclusiveButton.backgroundColor = UIColor.lightGrayColor()
        } else {
            self.getExclusiveButton.backgroundColor = UIColor.blackColor()
        }
        self.isGetExclusive = !self.isGetExclusive
        self.checkAllowSubmit()
    }
    
    @IBAction func pushOnSubmitButton(sender: UIButton) {
        self.delegate?.popupPressSubmit!(self)
    }
    @IBAction func pushOnCloseButton(sender: UIButton) {
        self.delegate?.popupPressClose!(self)
    }

    private func checkAllowSubmit() {
        self.submitButton.userInteractionEnabled = self.isAcceptPolicy && self.isGetExclusive ? true : false
    }
    
    private func setTheme() {
        self.titleLabel.font = self.font
        self.titleLabel.textColor = UIColor.blackColor()
        
        self.agreeSentence.font = self.font2
        self.agreeSentence.textColor = UIColor.darkGrayColor()
        
        self.getExclusiveSentence.font = self.font2
        self.getExclusiveSentence.textColor = UIColor.darkGrayColor()
        
        self.submitButton.titleLabel?.font = self.font
        self.submitButton.titleLabel?.textColor = UIColor.whiteColor()
        
        let attrs = [
            NSFontAttributeName : self.font2,
            NSForegroundColorAttributeName : UIColor.blackColor(),
            NSUnderlineStyleAttributeName : 1]
        
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string:"PRIVACY POLICY.", attributes:attrs)
        attributedString.appendAttributedString(buttonTitleStr)
        self.privacyPolicyButton.setAttributedTitle(attributedString, forState: .Normal)
    }
    
    private func displayPopupPolicy(){
        let myPopupViewController:PopupPolicyViewController = PopupPolicyViewController(nibName:"PopupPolicyViewController", bundle: nil)
        myPopupViewController.view.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        self.presentpopupViewController(myPopupViewController, animationType: .Fade, completion: { () -> Void in
            
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
