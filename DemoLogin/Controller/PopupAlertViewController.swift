//
//  PopupAlertViewController.swift
//  DemoLogin
//
//  Created by Atthachai Meethong on 10/17/15.
//  Copyright © 2015 Atthachai Meethong. All rights reserved.
//

import UIKit

class PopupAlertViewController: UIViewController {
    
    var delegate:PopupDelegate?
    
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var closeButton: UIButton!
    
    var message = String()
    let font: UIFont! = UIFont(name: "DINEngschriftStd", size: 20.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTheme()
        self.setMessage()
    }
    
    private func setTheme(){
        
        self.alertTitleLabel.font = self.font
        self.alertTitleLabel.textColor = UIColor.blackColor()
        
        self.messageTextView.font = self.font
        self.messageTextView.textColor = UIColor.darkGrayColor()
        self.messageTextView.backgroundColor = UIColor.clearColor()
        
        self.closeButton.titleLabel?.font = self.font
        self.closeButton.titleLabel?.textColor = UIColor.whiteColor()
        self.closeButton.backgroundColor = UIColor.blackColor()
    }
    
    private func setMessage(){
        self.messageTextView.text = self.message
    }
    
    @IBAction func pushOnCloseButton(sender: UIButton) {
        self.delegate?.popupPressClose!(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
