//
//  LoginViewController.swift
//  DemoLogin
//
//  Created by Atthachai Meethong on 10/14/15.
//  Copyright © 2015 Atthachai Meethong. All rights reserved.
//

import UIKit
import Alamofire

extension UIButton {
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.titleLabel?.frame = CGRectMake(0, 2.5, self.bounds.width, self.bounds.height)
        self.titleLabel?.baselineAdjustment = .AlignCenters
        self.titleLabel?.textAlignment = .Center
    }
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

class LoginViewController: UIViewController, UITextFieldDelegate, PopupDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var emailTextField: EGFloatingTextField!
    @IBOutlet weak var passwordTextField: EGFloatingTextField!
    @IBOutlet weak var confirmPasswordTextField: EGFloatingTextField!
    @IBOutlet weak var firstNameTextField: EGFloatingTextField!
    @IBOutlet weak var lastNameTextField: EGFloatingTextField!
    @IBOutlet weak var mobileNoTextField: EGFloatingTextField!
    @IBOutlet weak var createButton: UIButton!
    
    @IBOutlet weak var bottomSpaceCreateButton: NSLayoutConstraint!
    @IBOutlet weak var createButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var topSpaceEmailTextField: NSLayoutConstraint!
    var svos:CGPoint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            self.topSpaceEmailTextField.constant = 15.0
            self.bottomSpaceCreateButton.constant = 15.0
            self.createButtonHeight.constant = 40.0;
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
    
        self.title = "CREATE ACCOUNT"
        self.contentView.backgroundColor = UIColor.clearColor()
        self.scrollView.backgroundColor = UIColor(red:0.45, green:0.45, blue:0.45, alpha:1)
        self.setThemeTextFields([self.emailTextField, self.passwordTextField, self.confirmPasswordTextField, self.firstNameTextField, self.lastNameTextField, self.mobileNoTextField], placeholders: ["EMAIL","PASSWORD","CONFIRM PASSWORD", "FIRSTNAME", "LAST NAME", "MOBILE NO."])
        self.createButton.backgroundColor = UIColor(red:0.15, green:0.15, blue:0.15, alpha:1)
        self.createButton.titleLabel?.font = UIFont(name: "DINEngschriftStd", size: 20)
        
    }

    // MARK: - Theme
    private func setThemeTextFields(textFields:[EGFloatingTextField], placeholders:[String]) {
        var index:Int = 0
        for textField in textFields {
            textField.floatingLabel = true
            textField.setPlaceHolder(placeholders[index])
            textField.textColor = UIColor.whiteColor()
            textField.font = UIFont(name: "DINEngschriftStd", size: 20.0)
            textField.activeBorder.backgroundColor = UIColor.whiteColor()
            textField.tintColor = UIColor.whiteColor()
            
            if textField == self.emailTextField {
                textField.keyboardType = .EmailAddress
                textField.validationType = .Email
            }
            textField.delegate = self
            index++
        }
        
    }
    // MARK: - Action
    @IBAction func pushOnCreateButton(sender: UIButton) {
        
        self.hideAllKeyboard()
        
        if self.hasEmptyTextField() {
            self.displayAlert("has empty field")
        } else if (self.emailTextField.hasError == true) {
            self.displayAlert(self.emailTextField.errorMessage)
        }else if (self.passwordTextField.text?.characters.count < 6 ) {
            self.displayAlert("please enter more password")
        }else if (self.passwordTextField.text != self.confirmPasswordTextField.text) {
            self.displayAlert("confirm password invalid")
        } else {
            self.displayPopupSubmit()
        }
        
    }
    
    // MARK: - Private Method
    private func hasEmptyTextField()->Bool {
        let allTextField = [self.emailTextField, self.passwordTextField, self.confirmPasswordTextField, self.firstNameTextField, self.lastNameTextField, self.mobileNoTextField]
        for textfield in allTextField {
            if (textfield.text?.isEmpty)! {
                return true
            }
        }
        return false
    }
    
    private func hideAllKeyboard() {
        let allTextField = [self.emailTextField, self.passwordTextField, self.confirmPasswordTextField, self.firstNameTextField, self.lastNameTextField, self.mobileNoTextField]
        for textfield in allTextField {
            textfield.resignFirstResponder()
        }
    }
    
    private func handleResponeObject(responseObject:Dictionary<String, AnyObject>) {
        
        let status = responseObject["status"] as? Int
        if status == 1 {
            self.displayPopupPromoCode(responseObject["data"] as! Dictionary<String, AnyObject>)
        } else {
            let errorDict = responseObject["error"] as! Dictionary<String, AnyObject>
            self.displayAlert(errorDict["message"] as! String)
        }
    }
    
    // MARK: - Handle Keyboard
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.scrollView.contentSize = CGSize(width: 320, height: self.scrollView.frame.height + keyboardSize.height)
            
        }
    }
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.scrollView.contentSize = CGSize(width: 320, height: self.scrollView.frame.height - keyboardSize.height)
            
        }
    }
    // MARK: - UITextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField) {
        if textField == self.firstNameTextField || textField == self.lastNameTextField || textField == self.mobileNoTextField {
            self.svos = scrollView.contentOffset;
            
            var rc = textField.bounds
            rc = textField.convertRect(rc, toView: self.scrollView)
            var pt = rc.origin
            pt.x = 0
            pt.y -= 100
            self.scrollView.setContentOffset(pt, animated: true)
        }
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        } else if textField == self.passwordTextField {
            self.confirmPasswordTextField.becomeFirstResponder()
        }else if textField == self.confirmPasswordTextField {
            self.firstNameTextField.becomeFirstResponder()
        }else if textField == self.firstNameTextField {
            self.lastNameTextField.becomeFirstResponder()
        }else if textField == self.lastNameTextField {
            self.mobileNoTextField.becomeFirstResponder()
        } else {
             textField.resignFirstResponder()
        }
       
        return true
    }

    // MARK: - PopupDelegate
    func popupPressSubmit(sender: PopupSubmitViewController) {
        self.dismissPopupViewController(.Fade)
        self.requestCreateAccount()
    }
    func popupPressClose(sender: UIViewController) {
        self.dismissPopupViewController(.Fade)
    }
    // MARK: - Request
    func requestCreateAccount(){
        
        let urlString = "http://api.ccdoubleo.com/users/tempregister"
        let parameters = [  "lang"      : "en",
                            "email"     : self.emailTextField.text! as String,
                            "access"    : self.confirmPasswordTextField.text! as String,
                            "firstname" : self.firstNameTextField.text! as String,
                            "lastname"  : self.lastNameTextField.text! as String,
                            "mobile_no" : self.mobileNoTextField.text! as String]
        
        debugPrint(parameters)
        debugPrint("waiting for request . . .")
        Alamofire.request(.POST, urlString, parameters: parameters)
            .responseJSON { response in
                
                if (response.result.error != nil) {
                    debugPrint(response.result.error.debugDescription)
                } else if response.result.isSuccess {
                    if let jsonResult = response.result.value as? Dictionary<String, AnyObject> {
                        self.handleResponeObject(jsonResult)
                    } else {
                        debugPrint("parsing failure")
                    }
                } else {
                    debugPrint("request failure")
                }
                
        }
    }
   
    // MARK: - Display Popup
    func displayPopupPromoCode(dataDict:Dictionary<String, AnyObject>) {
        let myPopupViewController:PopupPromoCodeViewController = PopupPromoCodeViewController(nibName:"PopupPromoCodeViewController", bundle: nil)
        myPopupViewController.delegate = self
        myPopupViewController.dataDict = dataDict
        myPopupViewController.view.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        self.presentpopupViewController(myPopupViewController, animationType: .Fade, completion: { () -> Void in
            
        })
    }
    func displayPopupSubmit() {
        let myPopupViewController:PopupSubmitViewController = PopupSubmitViewController(nibName:"PopupSubmitViewController", bundle: nil)
        myPopupViewController.delegate = self
        myPopupViewController.view.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        self.presentpopupViewController(myPopupViewController, animationType: .Fade, completion: { () -> Void in
            
        })
    }
    
    func displayAlert(message:String){
        let myPopupViewController:PopupAlertViewController = PopupAlertViewController(nibName:"PopupAlertViewController", bundle: nil)
        myPopupViewController.delegate = self
        myPopupViewController.message = message
        myPopupViewController.view.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
        self.presentpopupViewController(myPopupViewController, animationType: .Fade, completion: { () -> Void in
            
        })
    }
    


}

