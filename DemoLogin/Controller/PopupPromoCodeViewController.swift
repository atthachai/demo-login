//
//  PopupPromoCodeViewController.swift
//  DemoLogin
//
//  Created by Atthachai Meethong on 10/17/15.
//  Copyright © 2015 Atthachai Meethong. All rights reserved.
//

import UIKit


class PopupPromoCodeViewController: UIViewController {
    
    var delegate:PopupDelegate?
    var dataDict = Dictionary<String,AnyObject>()
    
    @IBOutlet weak var promoTitleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!

    let font: UIFont! = UIFont(name: "DINEngschriftStd", size: 20.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTheme()
        self.setData()
    }

    private func setTheme() {
        self.promoTitleLabel.font = self.font
        self.promoTitleLabel.textColor = UIColor.blackColor()
        
        self.textView.font = self.font
        self.textView.textColor = UIColor.darkGrayColor()
        self.textView.backgroundColor = UIColor.clearColor()
        
        self.promoCodeLabel.font = self.font
        self.promoCodeLabel.textColor = UIColor.blackColor()
        self.promoCodeLabel.layer.borderWidth = 2.0
        self.promoCodeLabel.layer.borderColor = UIColor.blackColor().CGColor
        
        self.saveButton.titleLabel?.font = self.font
        self.saveButton.titleLabel?.textColor = UIColor.whiteColor()
        self.saveButton.backgroundColor = UIColor.blackColor()
    }
    
    private func setData() {
        self.promoTitleLabel.text   = self.dataDict["promotion_title"] as? String
        self.textView.text          = self.dataDict["promotion_desc"] as? String
        self.promoCodeLabel.text    = self.dataDict["code"] as? String
    }

    @IBAction func pushOnCloseButton(sender: UIButton) {
        self.delegate?.popupPressClose!(self)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
